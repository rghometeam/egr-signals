const models = require('./db').models;
const binance = require('node-binance-api');
const axios = require('axios');
const utils = require('./untils');

binance.options({
    APIKEY: '',
    APISECRET: '',
    useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
    test: false, // If you want to use sandbox mode where orders are simulated
    reconnect: true,
});


let allDbData = [];
let tradePairs = [];
let isChanged = false;
let statusSignal = {
    active: 'active',
    ended: 'ended',
    canceled: 'canceled',
};

let priceCounter = 0;


function getDbData() {
    models.Signals.findAll({
        where: {
            status:  statusSignal.active
        },
        include: [{ model: models.Targets }]
    }).then(signals => {
        // console.log(JSON.parse(signals));

        let dbPairs = []

        let data = []
        signals.forEach( item => {
            dbPairs.push(item.pair)
            // console.log(item.targets)
            let targets = []
            item.targets.forEach( target => {
                targets.push({
                    'id': target.id,
                    'value': target.value,
                    'is_hit': target.is_hit,
                    'number': target.number,
                    'signal_id': target.signal_id,
                })
            });

            data.push({
                'market': item.market,
                'reply_id': item.reply_id,
                'pair': item.pair,
                'id': item.id,
                'term': item.term,
                'buy_zone_one': item.buy_zone_one,
                'buy_zone_two': item.buy_zone_two,
                'buy_zone_flag': item.buy_zone_flag,
                'buy_price': item.buy_price,
                'stop_loss': item.stop_loss,
                'targets': targets,
                'link': item.link,
            })
        });

        allDbData = data

        checkDbChanges(dbPairs)
        // console.log(allDbData)

        startBinance();

        updatePrice();

    })
}

function updatePrice() {
    priceCounter += 1;

    if (priceCounter === 60) {
        console.log('update prices');

        allDbData.forEach(item => {
          binance.prices(item.pair, (error, ticker) => {
              console.log(ticker[item.pair]);

              let message = prepareMessage(item, ticker[item.pair]);

              editMessage(message, item.reply_id, item.pair);
          });
        });

        priceCounter = 0;
    }
}

function prepareMessage(item, currentPrice) {
    let pair = item.pair.split('BTC');
    const terms = {
        'long': '💫 Long Term',
        'middle': '🌤 Middle Term',
        'short': '🍎 Short Term'
    };

    let message = '⚡️⚡️ #' + pair[0] + ' - BTC ⚡️⚡️' + "\n";
    message += 'Market: ' + item.market + "\n";
    message += '**Current Price: ' + currentPrice + "**" + "\n";
    message += 'Buy Zone: ' + item.buy_zone_one + ' - ' + item.buy_zone_two + "\n";

    if (item.term) {
        message += 'Time: ' + terms[item.term] + "\n";
    }

    message += 'Targets: ' + "\n";

    item.targets.forEach(target => {
        message += target.number + ') ' + target.value + "\n";
    });

    message += 'Stop - loss: ' + item.stop_loss + "\n";

    if (item.link) {
        message += "\n" + 'Link: ' + item.link + "\n";
    }

    return message;
}

function startBinance() {
    if (isChanged) {
        binanceListener(tradePairs)
    }
}

// get trade pairs data
function binanceListener() {
    terminateBinance()

    if (!tradePairs.length) return;

    binance.websockets.candlesticks(tradePairs, "1m", function (candlesticks) {
        checkStopLoss(candlesticks.s, candlesticks.k.c);
        checkBuyHit(candlesticks.s, candlesticks.k.c);
        checkTargetHit(candlesticks.s, candlesticks.k.c);

    });
}

// stop binance websockets
function terminateBinance() {
    let endpoints = binance.websockets.subscriptions();

    for ( let endpoint in endpoints ) {
        binance.websockets.terminate(endpoint)
    }
}

function checkBuyHit(pair, value) {
    getRecordsFilteredBy(pair).forEach((item) => {
        if( item.buy_zone_flag !== true ) {
            if (value >= parseFloat(item.buy_zone_one) && value <= parseFloat(item.buy_zone_two)) {
                updateBuyZone(item, value)
                item.buy_zone_flag = true;
                item.buy_price = value;

                if (item.reply_id) {
                    let message = "👉The Coin Has now entered the Buy zone.\n";
                    sendMessage(message, item.reply_id);
                }
            }
        } else {
            if (value >= parseFloat(item.buy_zone_one) && value <= parseFloat(item.buy_zone_two) && item.buy_price < value) {
                updateBuyZone(item, value)
                item.buy_price = value;
            }
        }
    })
}

function checkTargetHit(pair,value) {
    getRecordsFilteredBy(pair).forEach((item) => {
        updateTargetStatus(item,value);
    });
}

function updateTargetStatus(item,value) {
    item.targets.forEach( (target) => {
        if (value >= target.value && target.is_hit !== true) {
            if (item.buy_zone_flag) {
                target.is_hit = true;

                let profit = utils.roundBy(
                    utils.targetProfit(parseFloat(item.buy_price), parseFloat(target.value)), 3
                );

                if (item.targets.indexOf(target) === item.targets.length - 1) {
                    // All targets achieved
                    let message = item.market + "\n";
                    message += '#' + utils.getCoin(item.pair) + '/BTC All target achieved 😎' + "\n";
                    message += 'Profit: ' + profit + '% 📈' + "\n";

                    sendMessage(message, item.reply_id);

                    updateRowIn(target.signal_id, statusSignal.ended, true);
                    updateRowIn(target.id, true, false);
                    deleteLocalRecord(item);
                } else {
                    // Single target achieved
                    let message = item.market + "\n";
                    message += '#' + utils.getCoin(item.pair) + '/BTC Target ' + target.number + ' ✅' + "\n";
                    message += 'Profit: ' + profit + '% 📈' + "\n";

                    sendMessage(message, item.reply_id);

                    updateRowIn(target.id, true, false);
                }
            } else {
                let message = item.market + "\n";
                message += '#' + utils.getCoin(item.pair) + '/BTC Canceled ❌' + "\n";
                message += 'Target achieved before entered the buy zone' + "\n";

                sendMessage(message, item.reply_id);

                updateRowIn(item.id, statusSignal.ended, true);
                updateRowIn(target.id, true, false);
                deleteLocalRecord(item);
            }

        }
    })
}


function checkStopLoss(pair,value) {
    getRecordsFilteredBy(pair).forEach((item) => {
        if (value <= item.stop_loss) {
            let averageBuy = parseFloat(item.buy_price);

            let message = item.market + "\n";
            message += '#' + utils.getCoin(item.pair) + '/BTC Stoploss ⛔' + "\n";
            message += 'Loss: ' + utils.roundBy(utils.targetLoss(averageBuy, value), 3) + "% 📉 \n";

            sendMessage(message, item.reply_id);

            updateRowIn(item.id, statusSignal.canceled, true);
            deleteLocalRecord(item);
        }
    })
}

function deleteLocalRecord( itemToDelete ) {
    allDbData.filter(function (record) {
        if (record === itemToDelete) {
            allDbData.splice(allDbData.indexOf(record),1)
        }
    })
}

function getRecordsFilteredBy(pair) {
    return allDbData.filter(function (item) {
        if (item.pair.toUpperCase() === pair.toUpperCase()) {
            return item
        }
    })
}

function updateBuyZone( item, value ) {
    models.Signals.update({
        buy_zone_flag: true,
        buy_price: value
    }, {
        where: {
            id: item.id
        }
    });
}

function updateRowIn(id, value, isSignals) {
    if (isSignals) {
        models.Signals.update({
            status: value,
        }, {
            where: {
                id: id
            }
        });
    } else {
        models.Targets.update({
            is_hit: value,
        }, {
            where: {
                id: id
            }
        });
    }

}

function checkDbChanges(dbPairs) {
    dbPairs = [...new Set(dbPairs)];

    if (JSON.stringify(dbPairs) === JSON.stringify(tradePairs)) {
        isChanged = false;
    } else {
        console.log('change');
        isChanged = true;
        tradePairs = dbPairs;
        console.log(tradePairs);
    }

}

function sendMessage(message, replyId) {
    axios.post('http://localhost:82/signals/reply', {
      message: message,
      reply_id: replyId
    })

    // .then(response => {
    //   let responseData = response.data;
    //
    //   // console.log(responseData);
    // })
}

function editMessage(message, messageId, pair) {
  axios.post('http://localhost:82/signals/edit', {
    message: message,
    message_id: messageId,
    pair: pair
  })
}

setInterval(getDbData, 5000);

