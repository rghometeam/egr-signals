/**
 * @param number
 * @param precision
 * @returns {number}
 */
exports.roundBy = function(number, precision) {
  let factor = Math.pow(10, precision);
  let tempNumber = number * factor;
  let roundedTempNumber = Math.round(tempNumber);

  return roundedTempNumber / factor;
};

/**
 * @param pair
 * @returns {*|string}
 */
exports.getCoin = function (pair) {
  let pairs = pair.split('BTC');

  return pairs[0];
};

/**
 * @param buy
 * @param target
 * @returns {number}
 */
exports.targetProfit = function (buy, target) {
  return Math.abs(((buy - target) / buy) * 100);
};

/**
 * @param buy
 * @param loss
 * @returns {number}
 */
exports.targetLoss = function (buy, loss) {
  return 100 - ((loss / buy) * 100);
};