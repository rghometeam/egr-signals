const Sequelize = require('sequelize');
const sequelize = new Sequelize('tg_manager', 'root', '541573', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,
    logging: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

const Signals = sequelize.define('signals', {
    market: {
        type: Sequelize.STRING
    },
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    pair: {
        type: Sequelize.STRING
    },
    buy_zone_one: {
        type: Sequelize.DECIMAL(15, 8)
    },
    buy_zone_two: {
        type: Sequelize.DECIMAL(15, 8)
    },
    buy_price: {
        type: Sequelize.DECIMAL(15, 8)
    },
    buy_zone_flag: {
        type: Sequelize.BOOLEAN
    },
    stop_loss: {
        type: Sequelize.DECIMAL(15, 8)
    },
    status: {
        type: Sequelize.STRING
    },
    reply_id: {
        type: Sequelize.INTEGER
    },
    term: {
        type: Sequelize.STRING
    },
    link: {
        type: Sequelize.STRING
    },
}, {
    timestamps: false,
});

const Targets = sequelize.define('targets', {
    value: {
        type: Sequelize.DECIMAL(15, 8)
    },
    is_hit: {
        type: Sequelize.BOOLEAN
    },
    number: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
});

Signals.hasMany(Targets, { foreignKey: 'signal_id' });
Targets.belongsTo(Signals, { foreignKey: 'signal_id' });

exports.dbInstance = sequelize;
exports.models = {
    Signals: Signals,
    Targets: Targets
};